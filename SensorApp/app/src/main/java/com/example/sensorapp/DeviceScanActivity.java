package com.example.sensorapp;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class DeviceScanActivity extends Activity {


    private boolean mScanning=false;
    private Handler handler = new Handler() ;
    private ArrayList<String> bleDevices ;
    private ArrayAdapter<String> adapter ;
    private Button scanbutton ;
    private ArrayList<BluetoothDevice> bDevices_list;
    // Stops scanning after 10 seconds.
    private static final long SCAN_PERIOD = 10000;
    private BluetoothScanService bluetoothScanService ;
    List listDevices ;

    //Lifecycle of Service
    private final ServiceConnection serviceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {

            bluetoothScanService = ((BluetoothScanService.LocalBinder) service).getService() ;
            if(bluetoothScanService == null) {
                System.out.println("not connected") ;
            }
            if (!bluetoothScanService.initialize()) {
                System.out.println("Unable to initialize Bluetooth");
                finish();
            }
            else{
                System.out.println("connesso al servizio");
            }


        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            bluetoothScanService = null;
        }
    };


    /**
     * Triggerato quando l'attivita' inizia, fa il bindind del servizio
     * e aspetta prima di fare la scansione in un thread separato se ancora non e' stato bindato
     * altrimenti inzia la scansione direttamente
     */
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    @Override
    protected void onStart() {
        super.onStart();
        Intent serviceIntent = new Intent(this, BluetoothScanService.class);
        bindService(serviceIntent, serviceConnection, BIND_AUTO_CREATE);

        if (bluetoothScanService == null) {
            final Thread thread = new Thread() {
                @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
                @Override
                public void run() {
                    while (bluetoothScanService == null) {
                    }
                    DeviceScanActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                        scanLeDevice(true);
                        }
                    });

                }
            };
            thread.start();
        }
        else {
            scanLeDevice(true);
        }





    }




    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listitem_device);


        //Creo ListView
        listDevices = new ArrayList();
        scanbutton = (Button) findViewById(R.id.buttonScan);
        bleDevices = new ArrayList<String>();
        bDevices_list = new ArrayList<BluetoothDevice>();
        adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, bleDevices);
        final ListView list = findViewById(R.id.ListDevices);
        list.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        list.setAdapter(adapter);

        //OnClickListener per elementi della lista

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                SparseBooleanArray position = list.getCheckedItemPositions();

                for (int j = 0; j < bleDevices.size();  j++) {

                    if (position.get(j)) {


                        if(!listDevices.contains(bleDevices.get(j))) {
                            System.out.println("aggiunto");
                            listDevices.add(bleDevices.get(j));
                        }
                        list.setItemChecked(j,false);

                    }
                }

                if(listDevices.size() >=2 ) {
                    System.out.println(listDevices);
                }



            }
        });

    }





    //triggerato dallo stop della corrente attivita', stoppa la scansione e unbinda il servizio
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    @Override
    protected void onStop() {
        super.onStop();
        scanLeDevice(false);
        unbindService(serviceConnection);
        bluetoothScanService = null ;
        handler.removeCallbacksAndMessages(null);
    }





    //inizia la scansione
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    public void scanLeDevice(final boolean enable) {



        if (enable) {
           scanbutton.setText("STOP");

            handler.postDelayed(new Runnable() {

                @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
                @Override
                public void run() {
                    mScanning = false;
                    scanbutton.setText("SCAN");
                    bluetoothScanService.stopScan(leScanCallback);
                }
            }, SCAN_PERIOD);

            mScanning = true;
            bleDevices.clear();
            listDevices.clear();
            adapter.notifyDataSetChanged();
            bluetoothScanService.startScan(leScanCallback);
        } else {
            scanbutton.setText("SCAN");
            mScanning = false;
            bluetoothScanService.stopScan(leScanCallback);
        }

    }




    //listener bottone scan, inizia o stoppa la scansione
    @SuppressLint("SetTextI18n")
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    public void StartScanButton(View view) {


        if(mScanning) {
          scanbutton.setText("SCAN");
            scanLeDevice(false);
        }
        else if (!mScanning){
            bleDevices.clear();
            listDevices.clear();
            bDevices_list.clear();
            scanbutton.setText("STOP");
            scanLeDevice(true);
        }
    }

    //ritorna il device associato a quell'indirizzo
    public BluetoothDevice getDeviceByAddress(String address){
        Iterator<BluetoothDevice> iterator = bDevices_list.listIterator();
        BluetoothDevice mydevice;


        while(iterator.hasNext()){

            mydevice = (BluetoothDevice) iterator.next();
            if(mydevice.getAddress().equals(address)){
                return mydevice;
            }

        }
        return null;
    }

    //callback associata alla scansione
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    private BluetoothAdapter.LeScanCallback leScanCallback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(final BluetoothDevice device, int rssi,
        byte[] scanRecord) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if(!bleDevices.contains(device.getAddress()) ) {
                        bleDevices.add(device.getAddress());
                        bDevices_list.add(device);
                        adapter.notifyDataSetChanged();
                    }
                }
            });
        }
    };



    public void Connetti(View view) {


        if(listDevices.size() >=2 ) {
            ArrayList<BluetoothDevice> clickedDevices = new ArrayList<>( );
            BluetoothDevice blDevice ;

            Iterator<String> iteratorAddress = listDevices.iterator();

            while(iteratorAddress.hasNext()) {
                blDevice = getDeviceByAddress(iteratorAddress.next()) ;
                clickedDevices.add(blDevice) ;
            }


            Intent intent = new Intent(DeviceScanActivity.this, DeviceControlActivity.class);
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList("ClickedDevices", clickedDevices);
            intent.putExtras(bundle);

            startActivity(intent);

        }
        else {
            Toast.makeText(this, "Devi selezionare piu' devices", Toast.LENGTH_SHORT).show();
        }




    }



}


