package com.example.sensorapp;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothProfile;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

public class DeviceControlActivity extends AppCompatActivity {

    private static final String TAG = DeviceControlActivity.class.getName();
    private BluetoothScanService bluetoothScanService ;
    private static BluetoothGattCharacteristic writeChara ;
    private ArrayList<BluetoothDevice> devices ;
    private static String addressOfRaspberry ;
    private static ArrayAdapter<String> adapter;
    private Intent serviceIntent;
    private Iterator<BluetoothDevice> iteratorDevices ;
    private static Map<String, BluetoothGatt> connectedDeviceMap;


    //Lifecycle of Service
    private final ServiceConnection serviceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            bluetoothScanService = ((BluetoothScanService.LocalBinder) service).getService();
            if (!bluetoothScanService.initialize()) {
                System.out.println("Unable to initialize Bluetooth");
                finish();
            }
            else{
                System.out.println("Connesso al servizio");

            }


        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            bluetoothScanService = null;
        }
    };


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    @Override
    protected void onStart() {
        super.onStart();
        System.out.println("on start attivato");
        bindService(serviceIntent, serviceConnection, BIND_AUTO_CREATE);

    }


    //Connecting device
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    private void connect() {

        while (iteratorDevices.hasNext()) {




                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        iteratorDevices.next().connectGatt(DeviceControlActivity.this, true, new BluetoothGattCallback() {
                                    public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
                                        super.onConnectionStateChange(gatt, status, newState);
                                        BluetoothDevice device = gatt.getDevice();
                                        String address = device.getAddress();


                                        switch (newState) {
                                            case BluetoothProfile.STATE_CONNECTED:
                                                Log.i("GattCallback", "connected");
                                                if (!connectedDeviceMap.containsKey(address)) {
                                                    connectedDeviceMap.put(address, gatt);
                                                }
                                                //deviceName_Address.setText((device.getName() != null) ? device.getName() : device.getAddress());
                                                gatt.discoverServices();
                                                break;
                                            case BluetoothProfile.STATE_DISCONNECTED:
                                                if (connectedDeviceMap.containsKey(address)) {
                                                    BluetoothGatt bluetoothGatt = connectedDeviceMap.get(address);
                                                    if (bluetoothGatt != null) {
                                                        bluetoothGatt.close();
                                                        bluetoothGatt = null;
                                                    }
                                                    connectedDeviceMap.remove(address);
                                                }
                                                Log.i("GattCallback", "Disconnected");
                                                break;
                                        }

                                    }


                                    @RequiresApi(api = Build.VERSION_CODES.N)
                                    @Override
                                    public void onServicesDiscovered(BluetoothGatt gatt, int status) {
                                        super.onServicesDiscovered(gatt, status);
                                        BluetoothDevice device = gatt.getDevice();
                                        String address = device.getAddress();


                                        if (status == BluetoothGatt.GATT_SUCCESS) {
                                            if (device.getAddress().equals("B8:27:EB:60:5B:57")) {
                                                if ((writeChara = bluetoothScanService.checkWriteCharactheristic(gatt.getServices())) != null) {
                                                    System.out.println("Can initiate the sending");

                                                    addressOfRaspberry = device.getAddress();
                                                    Intent sensorIntent = new Intent(DeviceControlActivity.this, SensorServiceForeground.class);
                                                    sensorIntent.putExtra("inputExtra", "SensorAPP NOTIFICATION");
                                                    ContextCompat.startForegroundService(DeviceControlActivity.this, sensorIntent);
                                                } else {
                                                    System.out.println("can't send data for device: " + address + ", I'm closing its Gatt");
                                                    finish();
                                                }
                                            } else {
                                                Log.i(TAG, "starting discovering for not rasp");
                                            }


                                        }


                                    }

                                }
                                , BluetoothDevice.TRANSPORT_LE);
                    }







        }
    }



    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.device_control_layout);
        Intent intent = getIntent();
        devices = intent.getExtras().getParcelableArrayList("ClickedDevices");

        iteratorDevices = devices.iterator() ;
        connectedDeviceMap = new HashMap<String, BluetoothGatt>();


        //deviceName_Address = findViewById(R.id.deviceControlAddress) ;
        serviceIntent = new Intent(this,BluetoothScanService.class) ;
        bindService(serviceIntent, serviceConnection, BIND_AUTO_CREATE);

        //beacon services start
        Intent beaconIntent = new Intent(DeviceControlActivity.this,BeaconScanForeground.class);
        ContextCompat.startForegroundService(DeviceControlActivity.this, beaconIntent);


        if (bluetoothScanService == null) {
            final Thread thread = new Thread() {
                @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
                @Override
                public void run() {
                    while (bluetoothScanService == null) {
                    }
                    DeviceControlActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            connect() ;
                        }
                    });

                }
            };
            thread.start();
        }
        else {
            connect() ;
        }

        //creo lista eventi
        adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1);
        final ListView list = findViewById(R.id.list_Connected) ;
        list.setAdapter(adapter);

    }




    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    public static void writeSensorEvent(double value) {

        BluetoothGatt bluetoothGatt = connectedDeviceMap.get(addressOfRaspberry);
        if( bluetoothGatt!=null) {
            String pattern = "hh:mm:ss";
            System.out.println(value);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, new Locale("it", "IT"));
            String date = simpleDateFormat.format(new Date());
            String stream = "" + date + " " + value;
            adapter.add(date + " : mossa : " + value);
            writeChara.setValue(stream);
            bluetoothGatt.writeCharacteristic(writeChara);
        }
        else {
            Log.e(TAG,"errore nello scrivere la carattersitica") ;
        }

    }





    @Override
    protected void onStop() {
        super.onStop();
        unbindService(serviceConnection);
        bluetoothScanService = null;
    }



}

