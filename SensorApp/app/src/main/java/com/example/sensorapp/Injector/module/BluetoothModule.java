package com.example.sensorapp.Injector.module;

import android.app.Application;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;



import dagger.Module;
import dagger.Provides;

@Module
public class BluetoothModule {

    private Application thisApp ;

    public BluetoothModule(Application application) {
        thisApp = application ;
    }

    @Provides
    public Application provideApplication() {
        return thisApp;
    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    @Provides
    public BluetoothManager provideBluetoothManager() {
        return (BluetoothManager) thisApp.getSystemService(Context.BLUETOOTH_SERVICE);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    @Provides
    public BluetoothAdapter provideBluetoothAdapter(@NonNull BluetoothManager btmanager) {
        return btmanager.getAdapter() ;
    }


}
