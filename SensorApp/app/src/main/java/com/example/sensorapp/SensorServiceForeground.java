package com.example.sensorapp;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.IBinder;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import java.sql.Timestamp;

public class SensorServiceForeground extends Service {

    private SensorManager sensorManager;
    int counter = 0;
    long oldTimestamp = 0;
    private Sensor accelerometer;
    private Sensor gyroscope;
    private Sensor step_detector;
    boolean activate = true;
    float xValue, yValue, zValue;
    double lastSent= 0;



    public static final String CHANNEL_ID = "ForegroundServiceChannel";
    @Override
    public void onCreate() {
        super.onCreate();


        //inizializzazione sensori

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        if (sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION) != null) {
            accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        }

        if (sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR) != null) {
            gyroscope = sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
        }

        if (sensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR) != null) {
            step_detector = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);
        }


    }



    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String input = intent.getStringExtra("inputExtra");
        createNotificationChannel();
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, 0);
        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("Foreground Service")
                .setContentText(input)
                .setSmallIcon(R.drawable.ic_stat_name)
                .setContentIntent(pendingIntent)
                .build();
        startForeground(1, notification);


        //QUA INIZIA IL LAVORO IN BACKGROUND
        final Thread thread = new Thread() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
            @Override
            public void run() {
                startSensorMonitoring();
            }
        } ;

        thread.start();
        return START_NOT_STICKY;
    }




    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Foreground Service Channel",
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    private void startSensorMonitoring() {

        SensorEventListener listener = new SensorEventListener() {

            /*
             LOGICA MONITORAGGIO:
                        1. Accelerometro: per piccoli movimenti è possibile registrare una variazione
                        sull'asse X compresa tra 0.8 e 1.3. Questa variazione si registra per movimenti
                        molto leggeri e generalmente privi di rotazioni. Questi movimenti posso essere
                        associati ad una qualità del sonno soddisfacente. Quando il movimento sull'asse X
                        registra dei valori molto più ampi vuol dire che il movimento è stato molto più netto.
                        -Per valori compresi tra 1.3 e 1.8 movimento medio
                        -Per valori maggiori di 1.8 movimento netto
                        Se oltre ad un movimente dell'asse X è possibile registrare anche una variaziazione
                        sull'asse Z (generalmente compreso tra 1 e 1.5) questo è segno che il soggetto
                        ha effettuato un movimento da un lato all'altro. Come per l'asse X è possibile definire
                        dei range anche per l'asse Z.
                        - Per valori compresi tra 1 e 1.5: ruotazione di metà corpo
                        - Per valori maggiori: ruotazione netta e completa
                        Per quanto riguarda l'asse Y, invece, il suo valore non è significativo ai fini
                        della valutazione del movimento del soggetto nel letto ma può essere utile per riuscire a
                        rilevare il momento in cui il soggetto si mette in piedi. Il valore di riferimento è
                        circa 1.5 (da verificare con sicurezza). Movimenti molto leggeri dell'asse Y, tuttavia,
                        rappresentano movimenti del braccio dall'alto verso il basso o viceversa (da decidere
                        se implementare o meno)

                        2. Step detector: il sensore permette di capire quando viene effettuato un passo.
                        Lo step detector si basa sui valori rilevati dall'accelerometro e non risulta essere
                        molto preciso nel suo calcolo. Spesso può succedere che mentre il soggetto effettua un movimento
                        significativo, questo viene considerato un passo. Per evitare questo è possibile considerare
                        un indice di errore: se il soggetto effettua più di un passo (almeno due) in un arco di tempo
                        allora questo implicherà che il paziente è effettivamente sveglio, in piedi ed in cammino
                        e rappresenterà la situazione critica.

                        3. Giroscopio: inutile ai fini del calcolo. I suoi valori indicano la posizione del soggetto.


            */

            @Override
            public void onSensorChanged(SensorEvent event) {

                if (event != null) {

                    if (event.sensor.getType() == Sensor.TYPE_LINEAR_ACCELERATION && activate) {

                        xValue = Math.abs(event.values[0]);
                        yValue = Math.abs(event.values[1]);
                        zValue = Math.abs(event.values[2]);

                        checkAccelerometerValues();

                    }

                }

                if (event.sensor.getType() == Sensor.TYPE_STEP_DETECTOR) {

                    checkStep();

                }

            }


            @Override
            public void onAccuracyChanged(Sensor sensor, int i) {

            }
        }; //FINE IMPLEMENTAZIONE LISTENER


        //Registrazione Sensori al Listener sopra implementato
        sensorManager.registerListener(listener, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(listener, gyroscope, SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(listener, step_detector, SensorManager.SENSOR_DELAY_NORMAL);

    }






    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    private void checkAccelerometerValues() {

        double index = (xValue + yValue + zValue) / 10;

        if ( (xValue >= 0.8 && xValue < 3.3) || index >= 0.2) {

            if(lastSent==index){
                return;
            }
            else{
                if(index>1)
                    index=1;
                DeviceControlActivity.writeSensorEvent(index);
                lastSent =index;
            }


        } else if (xValue > 3.3) {
            // x > 3.3 quindi movimento molto ampio
            DeviceControlActivity.writeSensorEvent(1);

        }


    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    private void checkStep() {
        //Momento in cui viene rilevato uno step
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        long tmp = timestamp.getTime();
        counter++;

        if (oldTimestamp != 0) {
            if (tmp - oldTimestamp <= 3000) {
                //differenza tra i passi minore di 3 secondi
                if (counter == 3) {
                    DeviceControlActivity.writeSensorEvent(1.0);
                    oldTimestamp = 0; //azzero per i prossimi cicli
                    counter = 0;
                } else {
                    //counter != 4 quindi devo continuare
                    oldTimestamp = tmp; //aggiorno il valore di riferimento per i prossimi  cicli
                }
            } else {
                //passati più di 3 secondi di differenza

                counter = 1; //reinizializzo il contatore
                oldTimestamp = tmp; //aggiorno con l'orario attuale per iniziare di nuovo il nuovo ciclo
            }

        } else {
            //timestamp = 0 quindi inizializzo
            oldTimestamp = tmp;
        }
    }


}
