package com.example.sensorapp;

import android.app.Application;

import com.example.sensorapp.Injector.ApplicationComponent;
import com.example.sensorapp.Injector.DaggerApplicationComponent;
import com.example.sensorapp.Injector.module.BluetoothModule;

import org.altbeacon.beacon.startup.RegionBootstrap;



//s:0-1=feaa,m:2-2=10,p:3-3:-41,i:4-20v



public class SensorApplication extends Application  {



    public ApplicationComponent appComponent = DaggerApplicationComponent.builder().bluetoothModule(
            new BluetoothModule(this)).build() ;


    @Override
    public void onCreate() {
        super.onCreate();


    }


}
