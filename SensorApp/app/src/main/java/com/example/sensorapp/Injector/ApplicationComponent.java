package com.example.sensorapp.Injector;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;

import com.example.sensorapp.BeaconScanForeground;
import com.example.sensorapp.BluetoothScanService;
import com.example.sensorapp.Injector.module.BluetoothModule;
import com.example.sensorapp.MainActivity;

import dagger.Component;

@Component(modules = BluetoothModule.class)
public interface ApplicationComponent {

    void inject(BluetoothScanService service) ;
    void inject(MainActivity main);
    void inject(BeaconScanForeground beaconScanForeground) ;

    BluetoothManager bluetoothManager();
    BluetoothAdapter bluetoothAdapter();





}
