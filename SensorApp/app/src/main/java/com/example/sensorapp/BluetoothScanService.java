package com.example.sensorapp;

import android.app.Application;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;

import androidx.annotation.RequiresApi;

import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;

public class BluetoothScanService extends Service {


    @Inject
     BluetoothAdapter bluetoothAdapter ;


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    public  void startScan(BluetoothAdapter.LeScanCallback leScanCallback) {
        bluetoothAdapter.startLeScan(leScanCallback);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    public void stopScan(BluetoothAdapter.LeScanCallback leScanCallback) {
        bluetoothAdapter.stopLeScan(leScanCallback);
    }






    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    public BluetoothGattCharacteristic checkWriteCharactheristic(List<BluetoothGattService> services ) {
        List<BluetoothGattCharacteristic> gattCharacteristics;
        Iterator<BluetoothGattService> iterator = services.listIterator();
        BluetoothGattService tmp;
        BluetoothGattCharacteristic chara ;

        while (iterator.hasNext()) {
            tmp = iterator.next();
            gattCharacteristics = tmp.getCharacteristics();

            System.out.println("Stampo il service");
            System.out.println(tmp.getUuid());


            if((chara=checkCharaProperties(gattCharacteristics)) != null ){
                return chara ;
            }
        }

        return null ;


    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    public BluetoothGattCharacteristic checkCharaProperties(List<BluetoothGattCharacteristic> chara) {
        Iterator<BluetoothGattCharacteristic> iterator = chara.listIterator();
        BluetoothGattCharacteristic tmp;
        while (iterator.hasNext()) {
            tmp = iterator.next();
            System.out.println("Stampo la Characteristic: " + tmp.getUuid());
            if ((tmp.getProperties() & BluetoothGattCharacteristic.PROPERTY_WRITE) > 0) {
                System.out.println("BluetoothGattCharacteristic has PROPERTY_WRITE, UUID:"+tmp.getUuid());
                return tmp ;
            }
        }

        return null;
    }




    private final LocalBinder localBinder = new LocalBinder() ;

    public boolean initialize(){
        return true;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return localBinder ;
    }


    @Override
    public void onCreate() {
        super.onCreate();

        ( (SensorApplication) getApplication()).appComponent.inject(this);

    }


    public class LocalBinder extends Binder {
        public BluetoothScanService getService() {
            return BluetoothScanService.this;
        }
    }


    @Override
    public boolean onUnbind(Intent intent) {
        // After using a given device, you should make sure that BluetoothGatt.close() is called
        // such that resources are cleaned up properly.  In this particular example, close() is
        // invoked when the UI is disconnected from the Service.

        return super.onUnbind(intent);
    }

}



