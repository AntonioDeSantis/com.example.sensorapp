package com.example.sensorapp;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.blueupbeacons.sdk.ble.Beacon;
import com.blueupbeacons.sdk.ble.Scanner;


import javax.inject.Inject;

public class BeaconScanForeground extends Service {

    @Inject
    BluetoothAdapter bluetoothAdapter ;
    private BluetoothScanService bluetoothScanService ;

    //Lifecycle of Service
    private final ServiceConnection serviceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {

            bluetoothScanService = ((BluetoothScanService.LocalBinder) service).getService() ;
            if(bluetoothScanService == null) {
                System.out.println("not connected") ;
            }
            if (!bluetoothScanService.initialize()) {
                System.out.println("Unable to initialize the service to scan beacons");
                onDestroy();
            }
            else{
                System.out.println("connesso al servizio scan");
            }


        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            bluetoothScanService = null;
        }
    };


    @Override
    public void onDestroy() {
        super.onDestroy();
    }



    public static final String CHANNEL_ID = "ForegroundServiceChannel";
    @Override
    public void onCreate() {
        super.onCreate();
        ( (SensorApplication) getApplication()).appComponent.inject(this);

    }


    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Foreground Service Channel",
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }



    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        createNotificationChannel();
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, 0);
        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("Foreground Beacon Service")
                .setContentText("Beacons scan....")
                .setSmallIcon(R.drawable.ic_stat_name)
                .setContentIntent(pendingIntent)
                .build();
        startForeground(1, notification);




        //QUA INIZIA IL LAVORO IN BACKGROUND

      final Thread thread= new Thread() {
           @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
           @Override
           public void run() {

               scanner.start(Scanner.Mode.LowPower);

           }
       };

       thread.start();

        return START_NOT_STICKY;
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    Scanner scanner = new Scanner(new Scanner.Handler() {
        private final String TAG = "Scanner.Handler";

        @Override
        public Integer rssiThreshold() {

            return null;
        }

        @Override
        public void onError(Scanner scanner, int error) {
            Log.d(TAG, "onError: " + String.valueOf(error));
        }

        @Override
        public void onStart(Scanner scanner) {
            System.out.println("wei giarda ca sta ncignamu");
        }

        @Override
        public void onStop(Scanner scanner) {
            System.out.println("Stoppato");
        }

        @Override
        public boolean accept(Beacon beacon) {
            return true;
        }

        @Override
        public void onBeaconDetected(Scanner scanner, final Beacon beacon) {
            System.out.println("Wei agghiu cchiatu u beacon");
            Log.d(TAG, beacon.toString());

        }
    });


}
